# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

# @file: Configurables.py
# @purpose: customized Configurable classes for AthenaRootComps
# @author: Sebastien Binet <binet@cern.ch>

### ---------------------------------------------------------------------------
from OutputStreamAthenaRoot import createNtupleOutputStream

### ---------------------------------------------------------------------------
## import the automatically generated Configurables
